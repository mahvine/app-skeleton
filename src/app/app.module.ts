import { BrowserModule } from '@angular/platform-browser';

import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MdlModule } from '@angular-mdl/core';

import { AppComponent } from './app.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { HomeComponent } from './home/home.component';
import { QuestsComponent } from './content/quests/quests.component';
import { LeaderboardComponent } from './content/leaderboard/leaderboard.component';
import { RewardsComponent } from './content/rewards/rewards.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'quests',       component: QuestsComponent },
  { path: 'leaderboards', component: LeaderboardComponent },
  { path: 'rewards', component: RewardsComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];


@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    HomeComponent,
    QuestsComponent,
    LeaderboardComponent,
    RewardsComponent
  ],
  imports: [
    BrowserModule,
    MdlModule,
    RouterModule.forRoot(
      appRoutes,
      { useHash: true } // <-- debugging purposes only
    ),
    
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent],  schemas: [ NO_ERRORS_SCHEMA ],
  exports: [
    RouterModule
  ]
})
export class AppModule { }
